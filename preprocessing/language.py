#!/usr/bin/python3.6
import json
import os
import torch

"""
Code inspired by:
- PyTorch Tutorials
- Keras Tokenizer
- ...
"""
from enum import Enum

class SpecialToken(Enum):
    SOS_idx = 0
    EOS_idx = 1
    UNK_idx = 2
    PAD_idx = 3

    SOS= "<SOS>"
    EOS= "<EOS>"
    UNK = "<UNK>"
    PAD= "<PAD>"

class Lang:

    def __init__(self, name, word2index=None, index2word = None):
        """
        :param name: language name, e.g. "eng"
        :param word2index: an already created word2index vocabulary
        :param index2word: an already created index2word vocabulary
        """
        self.name = name
        if word2index:
            self.word2index = word2index
        else:
            self.word2index = {}
        if index2word:
            self.index2word = index2word
        else:
            self.index2word = {0: SpecialToken.PAD, 1: SpecialToken.SOS, 2: SpecialToken.EOS,
                               3: SpecialToken.UNK}  # reverse index

        self.word2count = {}

        self.n_words = self.index2word.__len__()
        self._reduced = False

    def add_sentence(self, sentence):
       # print(sentence)
        for word in sentence.split(' '):
            self.add_word(word)

    def add_word(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.word2count[word] = 1
            self.index2word[self.n_words] = word
            self.n_words += 1
        else:
            self.word2count[word] += 1

    def reduce(self, min_count):
        #https://github.com/spro/practical-pytorch/blob/master/seq2seq-translation/seq2seq-translation-batched.ipynb
        if self._reduced: return
        self._reduced = True

        keep_words = []

        for k, v in self.word2count.items():
            if v >= min_count:
                keep_words.append(k)

        print('keep_words %s / %s = %.4f' % (
            len(keep_words), len(self.word2index), len(keep_words) / len(self.word2index)
        ))

        # Reinitialize dictionaries
        self.word2index = {}
        self.word2count = {}
        self.index2word = {0: SpecialToken.PAD, 1:  SpecialToken.SOS, 2: SpecialToken.EOS, 3:SpecialToken.UNK}
        self.n_words = 4  # Count default tokens

        for word in keep_words:
            self.add_word(word)


    def dump_to_json(self, path):
        word2idx_name = self.name + "_word2idx.json"
        idx2word_name = self.name + "_idx2word.json"
        path1 = os.path.join(path, word2idx_name)
        path2 = os.path.join(path, idx2word_name)

        with open(path1, 'w') as fp:
            json.dump(self.word2index, fp)

        with open(path2, 'w') as fp:
            json.dump(self.index2word, fp)

    def __len__(self):
        return self.word2count

    def __str__(self):
        return "Vocabulary %s with %s unique words" %(self.name, self.__len__())

    @staticmethod
    def build_vocabulary_from_sent(lang_name, normalized_sents_from_dataset):
        vocab = Lang(name=lang_name)
        print(vocab.name)
        for sent in normalized_sents_from_dataset:
            vocab.add_sentence(sent)
        print("Unique tokens: ", len(vocab.index2word))
        return vocab

    @staticmethod
    def load_json_dump(path, filename):
        pass




class Tensorizer(object):
    """
    This class handles both vocabularies and conversion to and from a tensor
    """
    def __init__(self, src_vocabulary:Lang, trg_vocabulary:Lang):
        """
        :param src_vocabulary: the source langugae vocabulary
        :param trg_vocabulary: the target language vocabulary
        """
        self.src_vocab = src_vocabulary
        self.trg_vocab = trg_vocabulary

    def sentence_to_idx(self, vocab, sentence):
        idx = []
        for word in sentence.split(' '):
            if word in vocab.word2index:
                idx.append(vocab.word2index[word])
            else:
                idx.append(SpecialToken.UNK_idx.value)
        return idx

    def sentence_to_idx_EOS(self, vocab, sentence):
        return [vocab.word2index[word] for word in sentence.split(' ')] + [SpecialToken.EOS_idx.value]


    def sentence_to_tensor(self, vocab, sentence):
        #practical pytorch: variable_from_sentence
        idx = self.sentence_to_idx(vocab, sentence)
        idx.append(SpecialToken.EOS_idx.value)
        #idx: [2, 73, 778, 1533]
        #reshaping(-1,1): tensor([[   2],
        #[  73],
       # [ 778],
       # [1533],
       # [   1]], device='cuda:0')
        return torch.tensor(idx, dtype=torch.int64, device="cuda" if torch.cuda.is_available() else "cpu").view(-1,1)

    def pair_to_tensor(self, src_vocab, trg_vocab, pair):
        #practical pytorch
        src_tensor = self.sentence_to_tensor(src_vocab, pair[0])
        trg_tensor = self.sentence_to_tensor(trg_vocab, pair[1])
        return (src_tensor, trg_tensor)

    def pad_sequence(self, seq, max_length):
        """
        :param seq:
        :param max_length:
        :return:
        """
        seq+=[SpecialToken.PAD.value for i in range(max_length-len(seq))] #Post padding

    def word_for_id(self, idx:int, src=True):
        """
        :param idx: the predicted idx
        :param vocab: the vocabulary
        :return: the word, if found in vocab, else <UNK>
        """
        if src:
            vocab = self.src_vocab
        else:
            vocab = self.trg_vocab
        word = vocab.word2index.get(idx)
        if word:
            return word
        else:
            return SpecialToken.UNK_idx.value

    def build_sentence_from_idx(self, vocab, idx):
        sentence = []
        for i in idx:
            word = vocab.index2word[i]
            if word:
                sentence.append(word)
            else:
                sentence.append(SpecialToken.UNK.value)
        return sentence


