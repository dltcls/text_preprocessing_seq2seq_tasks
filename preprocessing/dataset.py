import os
import pandas as pd
from torch.utils.data import Dataset, DataLoader
import numpy as np

#data_root_dir = "../data/"
split_dir = "splits"

class CSVDataset(Dataset):
    def __init__(self, src_lang, trg_lang, file_name, path_to_file, root_dir):
        """

        :param src_lang:
        :param trg_lang:
        :param file_name:
        :param path_to_file:
        """
        self.src_lang = src_lang
        self.trg_lang = trg_lang
        self.file_name = file_name
        self.data_root = root_dir
        #print(self.data_root)

        dataframe = pd.read_csv(os.path.join(path_to_file), encoding="utf-8", index_col=0) #index_col=0 does not create Unnamed column
        print(dataframe.head())
        self.df = dataframe
        self.src_lang_sents = self.df.src
        self.trg_lang_sents = self.df.trg



    def __getitem__(self, idx):
        """
        :param idx: the index of the translation in the dataset
        :return: the row with src and trg text
        """
        return self.df.iloc[idx]

    def __len__(self):
        """
        :return: the whole dataset length
        """
        return len(self.df)

    def get_all_src_sent(self):
        return self.src_lang_sents

    def get_all_trg_sent(self):
        return self.trg_lang_sents

    def split(self, test_ratio=0.30, verbose=True):
        """

        :param test_ratio:
        :param verbose:
        :return:
        """
        df:pd.DataFrame = self.df.copy(deep=True)
        print("Shuffling dataset for splitting....")
        np.random.shuffle(df.values)
        print("New shuffled dataset: ")
        print(df.head(10))
        dataset_length = len(df)
        n_train = int((dataset_length) * (1-test_ratio))
        n_test = int((dataset_length)*test_ratio)
        n_test_val = int(n_test//2)
        train_set = df[:n_train]
        val_set = df[n_train:n_train+n_test_val]
        test_set = df[n_train+n_test_val:]
        if verbose:
            print("Data splitting overview:")
            print("Train set")
            print(train_set.head(), len(train_set))
            print("Validation set")
            print(val_set.head(), len(val_set))
            print("Test set")
            print(test_set.head(), len(test_set))

        store_directory = os.path.join(self.data_root,split_dir)
        print(store_directory)
        tr = "train.csv"
        vl = "val.csv"
        ts = "test.csv"
        train_set.to_csv(os.path.join(store_directory, tr))
        val_set.to_csv(os.path.join(store_directory, vl))
        test_set.to_csv(os.path.join(store_directory, ts))
        print("Splitted csv files stored!")

    @staticmethod
    def setup_dataset_split_persist(root_dir, file_name, src_lang, trg_lang, test_ratio=0.3):
        """

        :param root_dir:
        :param file_name:
        :param src_lang:
        :param trg_lang:
        :param test_ratio:
        :return:
        """

        print(root_dir)
        print(os.getcwd())
        path_to_file = os.path.join(root_dir,file_name)
        print(path_to_file)
        dataset = CSVDataset(root_dir = root_dir, path_to_file=path_to_file, src_lang=src_lang, trg_lang=trg_lang,file_name=file_name)
        dataset.split(test_ratio=test_ratio)
        return dataset

    @staticmethod
    def make_dataloaders_from_split_files(src_lang, trg_lang, split_file_name, root_dir, batch_size, shuffle=True, drop_last=True):
        """
        :param src_lang: the source language name
        :param trg_lang: the target language name
        :param split_file_name: split file name, e.g. train.csv
        :param root_dir: the root directory, e.g. data/splits/
        :param batch_size: the batch size to be included in the dataloader
        :param shuffle: if set, batches are shuffled
        :param drop_last: if set, last is dropped
        :param device: device
        :return:
        """
        path_to_file = os.path.join(root_dir,split_file_name)
        dataset = CSVDataset(src_lang=src_lang, trg_lang=trg_lang, path_to_file=path_to_file)
        return DataLoader(dataset=dataset, batch_size=batch_size, shuffle=shuffle, drop_last=drop_last)


