import os
import torch
from enum import Enum
"""

Code inspired by:

- PyTorch Tutorial "Translation with a Sequence to Sequence Network and Attention": 
            https://pytorch.org/tutorials/intermediate/seq2seq_translation_tutorial.html 
            
- Practical PyTorch "Practical PyTorch: Translation with a Sequence to Sequence Network and Attention¶": 
            https://github.com/spro/practical-pytorch/blob/master/seq2seq-translation/seq2seq-translation.ipynb
            
- J. Brownlee, "How to Develop a Neural Machine Translation System from Scratch": 
            https://machinelearningmastery.com/develop-neural-machine-translation-system-keras/
"""

#Set if cuda is available or not
USE_CUDA = torch.cuda.is_available()

class SpecialToken(Enum):
    """
    Defines special tokens:
    SOS = Start of Sentence
    EOS = End of Sentence
    UNK = Unknown
    PAD = Padding
    """
    PAD_idx = 0
    SOS_idx = 1
    EOS_idx = 2
    UNK_idx = 3


    SOS= "<SOS>"
    EOS= "<EOS>"
    UNK = "<UNK>"
    PAD= "<PAD>"

class Lang(object):

    """
    This class defines a vocabulary for one language in the system.
    It contains data structures (dictionaries) to map word to their indices and indices to words.
    """

    def __init__(self, name, word2index=None, index2word = None):
        """
        :param name: language name, e.g. "eng"
        :param word2index: an already created word2index vocabulary
        :param index2word: an already created index2word vocabulary
        """
        self.name = name
        if word2index:
            self.word2index = word2index
        else:
            self.word2index = {}
        if index2word:
            self.index2word = index2word
        else:
            self.index2word = {0: SpecialToken.PAD, 1: SpecialToken.SOS, 2: SpecialToken.EOS,
                               3: SpecialToken.UNK}  # reverse index

        self.word2count = {}

        self.n_words = self.index2word.__len__()
        self._reduced = False

    def add_sentence(self, sentence):
        """
        Processes a sentence while adding its words to the vocabulary
        :param sentence: sentence
        """
        for word in sentence.split(' '):
            self.add_word(word)

    def add_word(self, word):
        """
        Adds a single word to the vocabulary
        :param word:
        """
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.word2count[word] = 1
            self.index2word[self.n_words] = word
            self.n_words += 1
        else:
            self.word2count[word] += 1


    def __len__(self):
        """
        :return: the length of the vocabulary
        """
        return self.word2count

    def __str__(self):
        """
        :return: String representation of the class
        """
        return "Vocabulary %s with %s unique words" %(self.name, self.__len__())

    @staticmethod
    def build_vocabulary_from_sent(lang_name, normalized_sents_from_dataset):
        """
        Builds the vocabulary for the first time
        :param lang_name: the name of the language, e.g. "eng", "deu"
        :param normalized_sents_from_dataset: a list of already cleaned sentences
        :return: the vocabulary for that language
        """
        vocab = Lang(name=lang_name)
        for sent in normalized_sents_from_dataset:
            vocab.add_sentence(sent)
        print("Unique tokens: ", len(vocab.index2word))
        return vocab


""" Vectorization methods """


# Return a list of indexes, one for each word in the sentence
def indexes_from_sentence(lang, sentence):
    return [lang.word2index[word] for word in sentence.split(' ')]

def variable_from_sentence(lang, sentence):
    indexes = indexes_from_sentence(lang, sentence)
    indexes.append(SpecialToken.EOS_idx.value)
    var = torch.LongTensor(indexes).view(-1, 1)
    if USE_CUDA: var = var.cuda()
    return var

def variables_from_pair(input_lang:Lang, output_lang:Lang, pair):
    input_variable = variable_from_sentence(input_lang, pair[0])
    target_variable = variable_from_sentence(output_lang, pair[1])
    return (input_variable, target_variable)


