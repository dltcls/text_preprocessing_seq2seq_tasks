from preprocessing.dataset import CSVDataset
from preprocess import *
from preprocessing.language import Lang, Tensorizer

if __name__ == '__main__':
    data_dir = "data"
    split_dir = os.path.join(data_dir, "splits")
    dataset_names = {"full": "rapid_data.csv", "10000": "data10000.csv"}

    vocab_dir = "preprocessing/vocab/"
    #files = [f for f in glob.glob(os.path.join(data_dir, "rapid_data.csv"))]
   # if files:
     #   print("Preprocessing already down, proceed with dataset creation")
   # else:
      #  print("Preprocess")
#
    # Create a dataset
    minidataset = CSVDataset.setup_dataset_split_persist(data_dir,dataset_names.get("10000"), "eng", "deu")

    # Create vectors and vocabularies for the main data set
    src_sents_list = minidataset.get_all_src_sent()
    trg_sents_list = minidataset.get_all_trg_sent()

    src_vocab = Lang.build_vocabulary_from_sent("eng", src_sents_list)
    trg_vocab = Lang.build_vocabulary_from_sent("deu", trg_sents_list)

   # src_vocab.dump_to_json(vocab_dir)
    #trg_vocab.dump_to_json(vocab_dir)

    #Vectorizer
    tensorizer = Tensorizer(src_vocab, trg_vocab)

    tensorTest = tensorizer.sentence_to_tensor(src_vocab, sentence="where is your truck")
    print("tensorTest:", tensorTest)
    print(tensorTest.shape)
    sentence = tensorizer.sentence_to_idx(src_vocab, sentence="where is your truck")
    print("sentence to idx result", sentence)
    print(tensorizer.build_sentence_from_idx(src_vocab, sentence))

