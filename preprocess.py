# normalization methods: https://pytorch.org/tutorials/intermediate/seq2seq_translation_tutorial.html
import datetime
import os
import re
import string
import unicodedata
import spacy
import pandas as pd
import time
from datetime import timedelta
from filter import umlaut_dictionary, eng_prefixes_dictionary, filter_pairs, is_question, english_prefixes

"""
Code inspired by:

- PyTorch Tutorial "Translation with a Sequence to Sequence Network and Attention": 
            https://pytorch.org/tutorials/intermediate/seq2seq_translation_tutorial.html 
            
- Practical PyTorch "Practical PyTorch: Translation with a Sequence to Sequence Network and Attention¶": 
            https://github.com/spro/practical-pytorch/blob/master/seq2seq-translation/seq2seq-translation.ipynb
            
- J. Brownlee, "How to Develop a Neural Machine Translation System from Scratch": 
            https://machinelearningmastery.com/develop-neural-machine-translation-system-keras/

In particular for the methdo 'rapid_preprocess':

- J. Browlee, "How to Prepare a French-to-English Dataset for Machine Translation":
            https://machinelearningmastery.com/prepare-french-english-dataset-machine-translation/
            
"""

root_dir = "data"

de_nlp = spacy.load("de")
en_nlp = spacy.load("en")


def unicode_to_ascii(s):
    """
    Convert the string s to ascii while removing accents
    'Mn' = mark non-spacing automatically removes accents, e.g. Umlaute
    This is a more rapid cleaning, the meaning of the sentence can be compromised
    (e.g. in German this removes the conditional form of werden and converts it to
    the past simple 'wurde(n)'
    :param s:
    :return: cleaned s

    """
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn'
    )

def tokenize(sentence, lang="eng", expand_contractions=True):
    sentence = sentence.lower()
    # excluded_tokens = r"""!"#$%&'()*+,-/:;<=>?@[\]^_`{|}~"""
    if lang == "eng":
        if expand_contractions:
            mapping = eng_prefixes_dictionary
            cleaned_sent = expand_contraction(sentence, mapping)
        else:
            cleaned_sent = normalize_string(sentence)
        tokens = [token.text.lower() for token in en_nlp(cleaned_sent) if
                  token.text not in string.punctuation and token.text not in string.digits]
        return tokens
    elif lang == "deu":
        if expand_contractions:
            mapping = umlaut_dictionary
            cleaned_sent = expand_contraction(sentence, mapping)
        else:
            cleaned_sent = normalize_string(sentence)
        tokens = [token.text.lower() for token in de_nlp(cleaned_sent) if
                  token.text not in string.punctuation and token.text not in string.digits]
        return tokens
    else:
        cleaned_sent = normalize_string(sentence)
        return cleaned_sent.split(" ")

def old_preprocess_lines(lines, src_lang="eng", src_reversed=False, expand_contractions=True, rapid=False):
    """
    :param lines: lines from file, see @read_file
    :param src_lang: file src language
    :param src_reversed: input reversed or not
    :return: preprocessed sentence in as a dictionary list
    """
    print("Preprocessing lines...")
    tokenized_pairs = []
    src_l = "eng" if src_lang == "eng" else "deu"
    trg_l = "deu" if src_lang == "eng" else "eng"
    if rapid:
        for i, (src_sent, trg_sent) in enumerate(lines):
            tokenized_pairs.append({"src_tokens": reverse_order(
                preprocess_sentence(src_sent, expand_contr, lang=src_l), reverse=src_reversed)
            if src_reversed else preprocess_sentence(src_sent, expand_contr, lang=src_l),
                                    "trg_tokens": preprocess_sentence(trg_sent, expand_contr, lang=trg_l)})
            if i % 10000 == 0:
                print("Processed sentence %s:" % str(i))
                print(tokenized_pairs[-1])
    else:
        #DEPRECATED
        for i, (src_sent, trg_sent) in enumerate(lines):
            # reverse_order(tokenize(src_sent,lang=src_l),reverse=src_reversed)
            tokenized_pairs.append({"src_tokens": reverse_order(
                tokenize(src_sent, lang=src_l, expand_contractions=expand_contractions), reverse=src_reversed)
            if src_reversed else tokenize(src_sent, lang=src_l, expand_contractions=expand_contractions),
                                    "trg_tokens": tokenize(trg_sent, lang=trg_l,
                                                           expand_contractions=expand_contractions)})
            if i % 10000 == 0:
                print("Processed sentence %s:" % str(i))
                print(tokenized_pairs[-1])
    print("Sentence preprocessing complete!")
    return tokenized_pairs


def preprocess_sentence(sentence, expand_contractions=False, lang="eng", filter_func=None):
    """
    Rapid preprocessing
    https://machinelearningmastery.com/prepare-french-english-dataset-machine-translation/
    Pipeline:
    - Expand contractions if needed
    - Apply some filters, e.g. filter out sentences starting with certain prefixes
    - Remove non printable chars
    - Remove punctuation
    - Normalize chars to latin chars (e.g. accents simply removed)
    - Splitting on white spaces (== tokenizing)
    - Lower case
    - Remove digits
    :param lines: lines to be preprocessed
    :return: cleaned lines
    """
    if expand_contractions:
        if lang == "eng":
            mapping = eng_prefixes_dictionary
            sentence = expand_contraction(sentence, mapping)
        elif lang == "deu":
            mapping = umlaut_dictionary
            sentence = expand_contraction(sentence, mapping)

    #Filtering function only applies on a sentence level
    #if filter_func:
        #sentence = filter_func(sentence)

    # prepare regex for char filtering
    re_print = re.compile('[^%s]' % re.escape(string.printable))
    # prepare translation table for removing punctuation
    table = str.maketrans('', '', string.punctuation)
    line = unicodedata.normalize("NFD", sentence).encode("ascii", "ignore")
    line = line.decode('UTF-8')
    # tokenize on white space
    line = line.split()
    # convert to lower case
    line = [word.lower() for word in line]
    # remove punctuation from each token
    line = [word.translate(table) for word in line]
    # remove non-printable chars form each token
    line = [re_print.sub('', w) for w in line]
    # remove tokens with numbers in them
    line = [word for word in line if word.isalpha()]
    return line




def expand_contraction(sentence, mapping):
    """
    Expands tokens in sentence given a contraction dictionary

    source: https://www.linkedin.com/pulse/processing-normalizing-text-data-saurav-ghosh

    :param sentence: sentence to expand
    :param mapping: contraction dictionary
    :return: expanded sentence
    """
    contractions_patterns = re.compile('({})'.format('|'.join(mapping.keys())), flags=re.IGNORECASE | re.DOTALL)

    def replace_text(t):
        txt = t.group(0)
        if txt.lower() in mapping.keys():
            return mapping[txt.lower()]

    expanded_sentence = contractions_patterns.sub(replace_text, sentence)
    return expanded_sentence


def normalize_string(sentence):
    """
    Inspired by: PyTorch Tutorials
    Used in combination to @unicode_to_ascii function
    :param sentence:
    :param replace_dict:
    :return:
    """
    s = sentence
    s = re.sub(r"([ß])", r"ss", s)  # unicode_to_ascii cannot handle ß

    s = unicode_to_ascii(s)
    s = re.sub(r"([.!?])", r" \1", s)
    s = re.sub(r"[^a-zA-Z0-9.!?]+", r" ", s)

    return s


def reverse_order(token_list, reverse=True):
    """
    :param token_list:
    :param reverse: Flag
    :return: reversed token list or token list if filter is not to apply
    """
    if reverse:
        token_list = token_list[::-1]
    return token_list


def read_file(path_to_file, line_separator="\n", sent_separator="\t", limit=None):
    """
    :param limit:
    :param path_to_file: path to file, e.g. project_name/data/file.txt
    :param line_separator: line separator in file, for Tatoeba Dataset "\n"
    :param sent_separator: separator source and target sentence, in Tatoeba "\t"
    :return: lines
    """
    print("Reading file from:", path_to_file)
    with(open(path_to_file)) as f:
        lines = f.readlines()
    lines = [line.replace(line_separator, "").lower().split(sent_separator) for line in lines]
    if limit:
        lines = lines[:limit]
    return lines, len(lines)


def preprocess_lines(lines, src_lang="eng", src_reversed=False, expand_contractions=True, len_tuple=None, filter_func_tuple = None):
    """
    :param lines: lines from file, see @read_file
    :param src_lang: file src language
    :param src_reversed: input reversed or not
    :return: preprocessed sentence in as a dictionary list
    """
    print("Preprocessing %s lines..." %len(lines))
    if len_tuple or filter_func_tuple:
        lines = filter_pairs(lines, len_tuple=len_tuple, filter_func = filter_func_tuple)

        #lines,samples = [filter_pairs(pair, len_tuple=len_tuple, filter_func=filter_func_tuple) for pair in lines]

    print("Sentences filtered, dataset reduced to %s samples" %str(len(lines)))

    cleaned_pairs = []
    expand_contr = expand_contractions
    src_l = "eng" if src_lang == "eng" else "deu"
    trg_l = "deu" if src_lang == "eng" else "eng"
    for i, (src_sent, trg_sent) in enumerate(lines):
        cleaned_pairs.append({"src": reverse_order(
            preprocess_sentence(src_sent, expand_contr, lang=src_l), reverse=src_reversed)
        if src_reversed else preprocess_sentence(src_sent, expand_contr, lang=src_l),
                                "trg": preprocess_sentence(trg_sent, expand_contr, lang=trg_l)})
        if i % 10000 == 0:
            print("Processed sentence %s:" % str(i))
            print(cleaned_pairs[-1])
    print("Sentence preprocessing complete!")
    return cleaned_pairs, len(cleaned_pairs)



def save_to_csv(cleaned_lines, store_path="."):
    """
    :param cleaned_lines:
    :param store_path:
    :return:
    """
    for idx, pair in enumerate(cleaned_lines):
        pair["src"] = " ".join(pair.pop("src"))
        pair["trg"] = " ".join(pair.pop("trg"))
    df = pd.DataFrame(cleaned_lines)
    print(df.head())
    df.to_csv(store_path)


def build_file_name(limit, src_reversed, len_tuple, filter_func, expand_contrac):

    name = "data"
    store_file_name = ""
    if limit:
        store_file_name += "_%d" %limit
    if src_reversed:
        store_file_name+= "_src_reversed"
    if len_tuple:
        store_file_name+="_by_len"
    if filter_func:
        store_file_name+="_by_%s" % filter_func.__name__

    if expand_contrac:
        store_file_name+="_standard"

    elif not expand_contr:
        store_file_name+="_rude"

    file_name = name+store_file_name+".csv"

    return file_name

def preprocess_pipeline(root_dir, file_name, src_lang, src_reversed=False, limit=None, expand_contractions=True,
                        rapid=True, len_tuple = (3,25), filter_func_tuple = None):

    path_to_file = os.path.join(root_dir, file_name)
    lines, samples = read_file(path_to_file=path_to_file, limit=limit)
    print("Dataset includes %s samples" % str(samples))

    #Check filter functions
    if len_tuple or filter_func_tuple:
        if limit:
            limit = None

    #start preprocessing lines by applying filtering, cleaning etc.
    preprocessed_lines, final_number = preprocess_lines(lines, src_lang=src_lang, src_reversed=src_reversed,
                                          expand_contractions=expand_contractions, len_tuple=len_tuple, filter_func_tuple=filter_func_tuple)


    # Persisting cleaned sentences as csv
    store_file_name = build_file_name(limit, src_reversed, len_tuple, filter_func_tuple, expand_contractions)
    print(store_file_name)

    stor_dir = os.path.join(root_dir, "preprocessed")
    store_path = os.path.join(stor_dir, store_file_name)
    save_to_csv(preprocessed_lines, store_path)

    return samples, final_number


def runtime(start):
    end_time = time.monotonic()
    run = timedelta(seconds=end_time - start)
    print("Preprocessing completed in hh:mm:ss: %s" % run)
    return run


if __name__ == "__main__":
    print("Start program at", os.getcwd())
    start = time.monotonic()
    file = "deu.txt"
    expand_contr = False
    src_reversed = False
    filter = False
    MIN = 3
    MAX = 25
    #len_tuple = (MIN, MAX)
    len_tuple = None
    limit = None
    filter_f = is_question

    lines, final_num = preprocess_pipeline(root_dir=root_dir, file_name=file, src_lang="eng", limit=limit,
                                src_reversed=src_reversed, expand_contractions=expand_contr, len_tuple=len_tuple, filter_func_tuple=filter_f)
    total = runtime(start)

    with open("prepro_runtime.txt", mode='a') as f:
        f.write("\n********************************************************")
        f.write("\nPreprocessing computed on %s" % (datetime.datetime.now()))
        f.write("\n")
        f.write("Total runtime for preprocessing: ")
        f.write(str(total))
        if expand_contr:
            f.write(
                "\nDataset preprocessed with standard preprocessing pipeline. This includes removal of diaeresis or contractions.")
        if filter:
            if len_tuple:
                f.write(
                    "\nFilter: Dataset only includes sentence of length between %s and %s words" % (MIN, MAX))
            if filter_f:
                f.write(
                    "\nFilter: %s" %filter_f.__name__)
        f.write("\nSource sentences reversed, e.g. Berlin living am i: %s" % str(src_reversed))
        f.write("\nDataset samples: %s" % str(final_num))
        f.write("\n********************************************************")
        f.close()


