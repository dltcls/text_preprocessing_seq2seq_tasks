# Text preprocessing for seq2seq tasks

This repo includes experiments and use cases for text preprocessing in seq2seq tasks.

Text preprocessing in such cases, include a standard preprocessing pipeline (file reading, filtering and tokenizing), creating Dictionaries for both source and targets (e.g. in a machine translation task you may want to handle dictionaries for both input and target language, but this approach also applies to other NLP tasks as well, like Q.A., g2p or chatbots). 
The creation of a Dictionary which handles the mapping between semantic units (e.g. "tokens") and their position in the dictionary (e.g. index!), the conversion word2tensor and tensor2word and the use of special tokens, like "SOS" (start of sentence), "EOS" (end of sentence), "UNK" (unknown token), "PAD" (padding), is **crucial**.

Preprocessing is tested on the Tatoeba Dataset ( https://www.manythings.org/anki/deu-eng.zip). The dataset includes 176.692 sentences.

### Preprocessing the raw data
 Preprocessing is computed on the raw txt file which is converted at the end to a csv-File and managed through other abstractions.
 The preprocessing pipeline is customizable. Allowed arguments:
 - Limit: It reduces the dataset to 'limit' samples. As sentences in the Tatoeba Dataset are ordered ascending by sentence length, limit should be applied at the beginning, to train the model on a small dataset
 - Filter Pairs: 'filter_p' applies a filter based on a minimum and a maximum sentence length. These measures can be passed as an argument, as well. By default, min lenght is set to 3 and max length is set to 25.
   If this combination is used, the dataset is reduced to 34242 samples!
   Please note: 'limit' and 'filter_p' do not have to be combined. If filter_p is set to True and an integer is passed as a limit, limit is automatically set to None!
- Tokenization: ...

I will consider these approaches:
1. Dataset and Dataloader abstractions by PyTorch + Vocabulary and Vectorizer classes
2. Torchtext by PyTorch
3. AllenNLP for PyTorch
4. Keras Tokenizer
5. ...
